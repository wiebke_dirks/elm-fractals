The following code will create a tree. In this example we generate all Points that we pass on to the view in the init. This means that update is never getting called and the whole tree will be drawn at once. The view will get a list of linePoints, each of which represents one line. A linePoint has a starting Position(fromX,fromY) and a target Position (toX,toY)
These linePoints are calculated using a recursive algorithm. Finally, we pass the list of linePoints to the view, where they are drawn.

Algorithm:
Given a starting point (x1,y1), we can calculate the target point(newPosX, newPosY) by:

         newPosX = x1 + (cos(degrees(theta)) * toFloat depth * 10.0)
         newPosY = y1 + (sin(degrees(theta)) * toFloat depth * 10.0)

This new line then gets appended to the list of existing linePoints:

        newLinePoints =  linePoints ++ [{fromX = x1, fromY = y1, toX = newPosX , toY = newPosY}]

Afterwards, we make a two recursive calls (one for the left branch and one for the right branch) in which we decrement the depth by 1 and change the angle that the new line is drawn at:

         calculateLinePoints newLinePoints newPosX newPosY (depth - 1) (theta - 20)
         ++ calculateLinePoints newLinePoints newPosX newPosY (depth - 1) (theta + 20)

See the code in action here:  https://ellie-app.com/5vKLyDLwmz7a1

        -- don't forget to import Canvas
        module Main exposing (..)
        
        import Browser
        import Canvas exposing (..)
        import Html exposing (Html)
        import Html.Attributes exposing (style)
        import Html exposing (..)
        import Color
        
        -- the Model holds a list of LinePoints (see below)
        type alias Model =
            {
                linePoints: List LinePoints
            }
        
        -- each linePoints consists of a starting position (fromX, fromY) and a target (toX,ToY)
        type alias LinePoints=
            {
               fromX : Float
             , fromY : Float
             , toX : Float
             , toY : Float
            }
        
        -- we don't have any subscription, the animation is drawn all at once
        type alias Msg = ()
        
        
        
        main : Program () Model Msg
        main =
            Browser.element
                { init =  init
                , view = view
                , update = update
                , subscriptions = \_ -> Sub.none
                }
        
        -- all work is done in init, so the update is empty
        update: Msg -> Model -> (Model, Cmd Msg)
        update msg model =
                        ( model, Cmd.none )
        
        -- we pass in the first line, together with the initial depth of 9 and an angle of -90.
        init: () -> (Model, Cmd Msg)
        init _ =
            ( {
            linePoints = calculateLinePoints [{fromX = width/2, fromY = height, toX = width/2, toY = height-100}] (width / 2) (height - 100) 9 -90
            }
            , Cmd.none)
        
        
        
        -- calculateLinePoints gets passed the list of existing linPoints, the current Position, the depth -- and the angle. By calling itself recursivly we calculate all linePoints in the tree.
        calculateLinePoints :  List(LinePoints) -> Float -> Float-> Int -> Float -> List(LinePoints)
        calculateLinePoints linePoints x1 y1 depth theta =
                if depth <= 0 then
                            linePoints
                else
                   let
                       newPosX = x1 + (cos(degrees(theta)) * toFloat depth * 10.0)
                       newPosY = y1 + (sin(degrees(theta)) * toFloat depth * 10.0)
                       newLinePoints =  linePoints ++ [{fromX = x1, fromY = y1, toX = newPosX , toY = newPosY}]
        
                    in
        
                       calculateLinePoints newLinePoints newPosX newPosY (depth - 1) (theta - 20)
                       ++ calculateLinePoints newLinePoints newPosX newPosY (depth - 1) (theta + 20)
        
        
        -- we pass in the model and draw all lines at once
        view : Model -> Html Msg
        view model =
        
                Canvas.toHtml (width, height)
                [ style "border" "10px solid rgba(0,0,0,0.1)" ]
                <|
                   List.map paint model.linePoints
        
        -- helper function for drawing the individual lines
        paint {fromX,fromY,toX,toY} =
             shapes  [stroke (Color.darkYellow)]
                [path (fromX,fromY)
                   [ lineTo (toX,toY)
                   ]
                ]
        
        -- witdth and heigth of the canvas
        width =
            800
        
        height =
            800


TASK:

EASY: Make the graph lean to one side by adjusting the degrees, change the colour or the thickness of the branches

HARD: Right now, all branches are calculated in init. Can you make an incremental version?
Hint: You need to subscribe to onAnimationFrameDelta again. I have a solution to this if you are interested.