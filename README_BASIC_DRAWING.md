**Using the joakin/elm-canvas library to create basic shapes**

https://package.elm-lang.org/packages/joakin/elm-canvas/latest/Canvas

Elm-canvas enables us to easily draw all kind of shapes!


To draw shapes, call toHtml in the view. You need to pass in the width and height of you Canvas, a list of Html.attributes and a list of Renderables. 

    toHtml : ( Int, Int ) -> List (Attribute msg) -> List Renderable -> Html msg

Let's take a look at one of our basic building blocks: shapes

    shapes : List Setting -> List Shape -> Renderable

An example of this is:  

    shapes [ fill Color.yellow ] [ circle ( 65, 64 ) 20 ] (see full code further down)

You pass in 
- a List of Settings (fill Color.yellow), similar to Html.Attribute
- a List of Shapes (circle ( 65, 64 ) 20)
and this will return something of type Renderable, which Canvas can the plot.

The librabry provides functions for basic shapes like circles and rectangles, and if you want to draw something more complicated, you can construct your own shape by using functions like lineTo.
If you want to build up you animation step by step and not display it all at once, you could subscibe to onAnimationFrameDelta, which will then update you model in certain intervals.

The code below draws some basic shapes, displays a text and changes the size of the text over time making use of the subscription to onAnimationFrameDelta.
To see it in action, have a look at:
https://ellie-app.com/5NpNVYJsL9Qa1

        module Main exposing (Model, Msg, height, main, update, view, width)
        
        import Browser
        import Browser.Events exposing (onAnimationFrameDelta)
        import Canvas exposing (..)
        import Color
        import Html exposing (Html, div)
        import Html.Attributes exposing (style)
        import Random
        
        
        type Msg
            = Frame Float
        
        
        type alias Model =
            { count : Float }
        
        
        main : Program () Model Msg
        main =
            Browser.element
                { init = \() -> ( { count = 0 }, Cmd.none )
                , view = view
                , update = update
                , subscriptions = \_ -> onAnimationFrameDelta Frame
                }
        
        
        update : Msg -> Model -> ( Model, Cmd Msg )
        update msg model =
            if model.count < 80 then
                ( { model | count = model.count + 1 }, Cmd.none )
        
            else
                ( { model | count = model.count + 0 }, Cmd.none )
        
        
        width =
            450
        
        
        height =
            400
        
        
        view : Model -> Html Msg
        view model =
            Canvas.toHtml
                ( width, height )
                [ style "border" "10px solid rgba(0,0,0,0.1)" ]
                [ shapes [ fill Color.white ] [ rect ( 0, 0 ) width height ]
                , shapes [ stroke Color.lightCharcoal, lineWidth 5 ]
                    [ path ( 225, 10 )
                        [ lineTo ( 440, 390 )
                        , lineTo ( 10, 390 )
                        , lineTo ( 225, 10 )
                        ]
                    ]
                , shapes [ fill Color.blue ] [ circle ( 45, 44 ) 30 ]
                , shapes [ fill Color.yellow ] [ circle ( 65, 64 ) 20 ]
                , shapes [ fill Color.red ] [ rect ( 150, 150 ) 280 80 ]
                , text
                    [ align Right
                    , font { size = round model.count, family = "sans-serif" }
                    , lineWidth 1
                    , stroke Color.blue
                    , fill Color.green
                    ]
                    ( 450, 380 )
                    "Hello world"
                ]
