Creating Fractals in Elm using Canvas: Elm-meetup Cambridge


I drew a Sirpinski triangle using a random number generator.
See https://ellie-app.com/5tPDNqLL4ZZa1 for the code in action.

The algorithms is simple:``

 1   Choose 3 points to form a triangle.
 2   Randomly choose one point inside of the triangle. This is you current position
 3   Randomly choose one corner of the triangle.
 4   Move half way between your position and the corner.
 5   Mark you position.
 6   Repeat  2-5. 

You will see a Sirpinski triangle forming.


For the random selection of a corner, I used a Random Number Generator to mimick rolling a die. For a detailed description of how random numbers work in elm look here (https://elmprogramming.com/commands.html)
By subscribing to onAnimationFrameDelta which triggers about 60 times per second you can step through the animation and see the triangle building up.

You can use joakin/elm-canvas to create easy visualisations in Elm. 


    -- don't forget to import Canvas and Random
    module Main exposing (..)
    
    import Browser
    import Browser.Events exposing (onAnimationFrameDelta)
    import Canvas exposing (..)
    import Color
    import Html exposing (Html, div)
    import Html.Attributes exposing (style)
    import Random
    import Html exposing (..)


    -- the model holds the current position: pox, posY, and the result of the dice roll.
    type alias Model =
        { posX : Float
        , posY : Float
        , diceFace : Int
        }
    
    
    -- the init represents step 2 and 3 of the algorithm, we choose a position in the triangle and a corner (represented by the diceFace)
    main : Program () Model Msg
    main =
        Browser.element
            { init = \() -> ( { posX = 225, posY = 10, diceFace = 1}, Cmd.none )
            , view = view
            , update = update
            , subscriptions = \model -> onAnimationFrameDelta Frame
            }
    
    -- messages that can get triggered
    type Msg
        = Frame Float
        | NewRandomNumber Int
    
    
    -- the update takes care of generating new random numbers whenever the Frame is updated
    update: Msg -> Model -> (Model, Cmd Msg)
    update msg model =
            case msg of
                Frame _ ->
                    ( model, Random.generate NewRandomNumber (Random.int 1 6) )
    
                NewRandomNumber number ->
                    let
                       (newX, newY) = calculatePosition number model
                    in
                    ( {model |
                      diceFace = number
                      , posX = newX
                      , posY = newY
                    }, Cmd.none )
    
    
    -- choose a corner depending on the result of the diceroll and calculate a new position halfway to that corner
    calculatePosition : Int -> Model -> (Float, Float)
    calculatePosition diceFace {posX, posY} =
    
    
            if (diceFace == 1 || diceFace == 2) then
    
                ((posX + triangleTopX) / 2, (posY + triangleTopY) / 2)
    
            else if (diceFace == 3 || diceFace == 4) then
               ((posX + triangleLeftX) / 2, (posY + triangleLeftY) / 2)
    
    
            else if (diceFace == 5 || diceFace == 6) then
                ((posX + triangleRightX) / 2, (posY + triangleRightY) / 2)
            else
                (posX , posY )
    
    
    
    -- "toHtml is almost like creating other Html elements. We need to pass (width, height) in pixels, a list of Html.Attribute, and finally instead of a list of html elements, we pass a List Renderable"
    -- (https://package.elm-lang.org/packages/joakin/elm-canvas/latest/)
    -- the view will plot the current position as a small circle. Previous cirles aren't removed so that we don't have to redraw those.
    
    view : Model -> Html Msg
    view { posX, posY } =
           Canvas.toHtml
                ( width, height )
                [ style "border" "10px solid rgba(0,0,0,0.1)" ]
                [ shapes  [ fill Color.green ] [ circle ( posX, posY ) 5 ]]
    
    
    
    -- finally we need some constants for width, heigth and the corners of the triangle
    width =
        450
    
    
    height =
        400
    
    
    triangleTop =
        (225, 10)
    
    triangleTopX = 225
    triangleTopY = 10
    triangleLeftX = 10
    triangleLeftY = 390
    triangleRightX = 440
    triangleRightY = 390
    


TASK: Can you create a Barnsley Fern by applying to followin transformation?

Create this fractal fern, using the following transformations:

    ƒ1   (chosen 1% of the time)

        xn + 1 = 0
        yn + 1 = 0.16 yn

    ƒ2   (chosen 85% of the time)

        xn + 1 = 0.85 xn + 0.04 yn
        yn + 1 = −0.04 xn + 0.85 yn + 1.6

    ƒ3   (chosen 7% of the time)

        xn + 1 = 0.2 xn − 0.26 yn
        yn + 1 = 0.23 xn + 0.22 yn + 1.6

    ƒ4   (chosen 7% of the time)

        xn + 1 = −0.15 xn + 0.28 yn
        yn + 1 = 0.26 xn + 0.24 yn + 0.44.

Starting position: x = 0, y = 0 

Hint: If you set your Canvas to have a height and witdth of 640 pixel, apply the following translation to your view:
     
       [  shapes  [ fill Color.green ] [ circle ( width/2 + 60 * posX, height - (60*posY) ) 3 ].
     
You will also need to change the DiceRoll to eg generate numbers between 1-100. Other than that only your calculatePosition Function will change.

